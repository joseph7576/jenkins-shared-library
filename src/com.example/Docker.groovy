#!/usr/bin/env groovy
package com.example

class Docker implements Serializeble {
    
    def script

    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        
        srcipt.echo "---> Building the docker image..."
        srcipt.withCredentials([
            srcipt.usernamePassword(credentialsId: "docker-hub-joseph7576", usernameVariable: "DOCKER_USER", passwordVariable: "DOCKER_PASS") 
        ]) {
            srcipt.sh "docker build -t $imageName ."

            srcipt.echo "---> Pushing docker image to repository..."
            srcipt.sh "echo $script.DOCKER_PASS | docker login -u $script.DOCKER_USER --password-stdin"
            srcipt.sh "docker push $imageName"
        }
    }
}
